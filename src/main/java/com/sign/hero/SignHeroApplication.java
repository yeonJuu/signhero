package com.sign.hero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SignHeroApplication {

	public static void main(String[] args) {
		SpringApplication.run(SignHeroApplication.class, args);
	}

}
